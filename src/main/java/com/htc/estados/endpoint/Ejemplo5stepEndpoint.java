package com.htc.estados.endpoint;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.htc.estados.lista.GetStateRequest;
import com.htc.estados.lista.GetStateResponse;
import com.htc.estados.repository.RespositoryEstados;

@Endpoint
public class Ejemplo5stepEndpoint {
    private static final String NAMESPACE_URI = "http://lista.estados.htc.com";

    private RespositoryEstados respositoryEstados;

    @Autowired
    public Ejemplo5stepEndpoint(RespositoryEstados repositoryEstados) {
        this.respositoryEstados = repositoryEstados;
    }

    
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getStateRequest")
    @ResponsePayload
    public GetStateResponse getState(@RequestPayload GetStateRequest request) {
        GetStateResponse response = new GetStateResponse();
        
        response.setEstado(respositoryEstados.findState(request.getId()));
      
       
        return response;
    }
    
  
}