package com.htc.estados.repository;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.htc.estados.lista.Estado;


import javax.annotation.PostConstruct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class RespositoryEstados{
    private static final Map<Integer,Estado> states = new HashMap<>();
    
    //private static final List<Estados> listCusto = new ArrayList<>();
    
    private List<Estado> listaEstados = new ArrayList<>();

    @PostConstruct
    public void initData() {
    	Estado ap = new Estado();
        ap.setId(1);
        ap.setCapital("Amaravathi");
        ap.setNombre("AndhraPradesh");
        ap.setLenguaje("Telugu");
        ap.setPoblacion(52000000);
        states.put(ap.getId(),ap);

        Estado tl = new Estado();
        tl.setId(2);
        tl.setCapital("Hyderabad");
        tl.setNombre("Telanga");
        tl.setLenguaje("Telugu");
        tl.setPoblacion(52000000);
        states.put(tl.getId(),tl);

        Estado ka = new Estado();
        ka.setId(3);
        ka.setCapital("Bangalore");
        ka.setNombre("Karnataka");
        ka.setLenguaje("Kannada");
        ka.setPoblacion(52000000);
        states.put(ka.getId(),ka);

        Estado tn = new Estado();
        tn.setId(4);
        tn.setCapital("Chennai");
        tn.setNombre("Tamilnadu");
        tn.setLenguaje("Tamil");
        tn.setPoblacion(52000000);
        states.put(tn.getId(),tn);
    	
    	/*Estados ap = new Estados();
        ap.setId(1);
        ap.setCapital("Amaravathi");
        ap.setNombre("AndhraPradesh");
        ap.setLenguaje("Telugu");
        ap.setPoblacion(52000000);
        listaEstados.add(ap);

        Estados tl = new Estados();
        tl.setId(2);
        tl.setCapital("Hyderabad");
        tl.setNombre("Telanga");
        tl.setLenguaje("Telugu");
        tl.setPoblacion(52000000);
        listaEstados.add(tl);

        Estados ka = new Estados();
        ka.setId(3);
        ka.setCapital("Bangalore");
        ka.setNombre("Karnataka");
        ka.setLenguaje("Kannada");
        ka.setPoblacion(52000000);
        listaEstados.add(ka);

        Estados tn = new Estados();
        tn.setId(4);
        tn.setCapital("Chennai");
        tn.setNombre("Tamilnadu");
        tn.setLenguaje("Tamil");
        tn.setPoblacion(52000000);
       listaEstados.add(tn);*/
    }

    public Estado findState(Integer id) {
        Assert.notNull(id, "State id must not be null");
        return states.get(id);
    }
    
    
}
