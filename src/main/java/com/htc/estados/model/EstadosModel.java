package com.htc.estados.model;

import java.io.Serializable;
import java.util.List;

public class EstadosModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String capital;
	private String nombre;
	private String lenguaje;
	private int poblacion;
	private List<EstadosModel> listaEstados;
	
	
	public EstadosModel() {
		super();
	}


	public EstadosModel(String id, String capital, String nombre, String lenguaje, int poblacion, List<EstadosModel> listaEstados) {
		super();
		this.id = id;
		this.capital = capital;
		this.nombre = nombre;
		this.lenguaje = lenguaje;
		this.poblacion = poblacion;
		this.listaEstados = listaEstados;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getCapital() {
		return capital;
	}


	public void setCapital(String capital) {
		this.capital = capital;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getLenguaje() {
		return lenguaje;
	}


	public void setLenguaje(String lenguaje) {
		this.lenguaje = lenguaje;
	}


	public int getPoblacion() {
		return poblacion;
	}


	public void setPoblacion(int poblacion) {
		this.poblacion = poblacion;
	}


	public List<EstadosModel> getListaEstados() {
		return listaEstados;
	}


	public void setListaEstados(List<EstadosModel> listaEstados) {
		this.listaEstados = listaEstados;
	}


	@Override
	public String toString() {
		return "Estados [id=" + id + ", capital=" + capital + ", nombre=" + nombre + ", lenguaje=" + lenguaje
				+ ", poblacion=" + poblacion + ", listaEstados=" + listaEstados + "]";
	}
	
	
	

}
